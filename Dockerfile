FROM centos as builder

WORKDIR /usr/local/src/docker

RUN yum update -y && yum install -y ca-certificates
RUN yum install --downloadonly --downloaddir=/usr/local/src socat
RUN rpm2cpio /usr/local/src/*.rpm | cpio -idmv
RUN useradd socat


FROM scratch

COPY --from=builder /etc/ssl/certs /etc/ssl/certs
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /usr/local/src/docker /

LABEL name="socat"
LABEL author="Schubert Anselme <sanselme@icloud.com>"
LABEL maintainer="Schubert Anselme <sanselme@icloud.com>"
LABEL version="1.7.3.2"
LABEL summary="socat - Multipurpose relay (SOcket CAT)"
LABEL description="Socat is a command line based utility that establishes two bidirectional byte streams and transfers data between them. Because the streams can be constructed from a large set of different types of data sinks and sources (see address types), and because lots of address options may be applied to the streams, socat can be used for many different purposes."

USER socat
ENTRYPOINT [ "/usr/bin/socat" ]
